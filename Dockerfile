FROM google/cloud-sdk:latest

WORKDIR /
RUN apt-get install git-core
RUN git clone https://gitlab.com/franciscogd/gcp-provisioning.git gcp && \
    /gcp/bin/setup.sh

EXPOSE 22 443 9418

ENTRYPOINT ["update"]
ENTRYPOINT ["provision"]
