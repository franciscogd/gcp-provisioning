# GCP Provisioning
To use this docker container you need a valid GCP service account.

## Docker Pull from Talend Registry
`$ docker pull us.self-service.talend.com:443/devops/gcp-provisioning:v1`<br/>
**Note**: valid credentials are needed to pull from the Talend registry.

## Docker Build
`$ git clone https://github.com/franciscogd/gcp-provisioning.git`<br/>
`$ cd gcp-provisioning`<br/>
`$ docker build -t gcp-provisioning .`

## Docker Run
Save your service account under <host_mount_point>/gcloud-util-service-account.json<br/>
**Note**: rename it to match "gcloud-util-service-account.json"<br/>
`$ docker run -it -v <host_mount_point>:/talend/ gcp-provisioning`

## Pseudo-TTY
`$ docker run -it --entrypoint bash <container_id>`

### Update
The tool updates automatically at runtime. But you can fetch the latest updates
from a running container:<br/>
`root@5f3dc4e25bc5:/# update`

### Provision
You can start the tool again by invoking the tool from the pseudo-TTY:<br/>
`root@5f3dc4e25bc5:/# provision`
