#!/bin/bash

source /gcp/bin/utils.sh
# set -e
date_format="%m-%d-%Y" # e.g. "03-08-2018"
offset="+14 days"
offset_mac="+2w" # two weeks offset
# week_day="+fri" # day of the week to archive

# ARCHIVE_DATE=$(date -v$offset -v$week_day +$date_format)
UNAME=$(uname -s)
if [[ $UNAME == 'Darwin' ]]; then
  # macOS
  ARCHIVE_DATE=$(date -v"$offset_mac" +"$date_format")
elif [[ $UNAME == 'Linux'  ]]; then
  if [ -f '/etc/redhat-release' ]; then
    # RHEL/CentOS
    ARCHIVE_DATE=$(date -d"$offset" +"$date_format")
  elif [ -f '/etc/lsb-release' ]; then
    # Debian/Ubuntu
    ARCHIVE_DATE=$(date -d"$offset" +"$date_format")
  elif [[ -f '/etc/SuSE-release' ]]; then
    # SUSE
    ARCHIVE_DATE=$(date -d"$offset" +"$date_format")
  fi
fi

VNC='vnc-5901'
HTTP='http-server'
HTTPS='https-server'
SERVICE_ACCOUNT='gcloud-util-service-account.json'

function main() {
  gauth $SERVICE_ACCOUNT
  getUsername
  readSalesforceCase
  readRegion
  readTimeZone
  readImage

  SCHEDULER="none-1800-$TIME_ZONE-all"
  LABELS="scheduler=$SCHEDULER,archive-date=$ARCHIVE_DATE,owner=$USERNAME"
  BASE_NAME="$USERNAME-$OS-$SOFTWARE-$SALESFORCE_CASE"
  INSTANCE_NAME="vm-$BASE_NAME"
  IP_NAME="ip-$BASE_NAME"

  printValues

  # GCP allows a max of 63 characters
  instance_length=${#INSTANCE_NAME}
  if [[ $instance_length > 63 ]]; then
    e_note "Instance name may be at most 63 characters."
    # INSTANCE_NAME is "vm-$USERNAME-$OS-$SOFTWARE-$SALESFORCE_CASE"
    extra_length=$((instance_length-63))
    software_length=${#SOFTWARE}
    correct_length=$((software_length-extra_length))
    SOFTWARE=${SOFTWARE:0:correct_length};
    INSTANCE_NAME="vm-$USERNAME-$OS-$SOFTWARE-$SALESFORCE_CASE"
    e_note "It has been truncated to: $INSTANCE_NAME"
  fi

  seek_confirmation "Do you want to create $INSTANCE_NAME?"

  if is_confirmed; then
    e_success "Creating instance in $PROJECT project."
    local createInstance_results="$(fdisk -l)"
    local createIP_results="$(fdisk -l)"
    createIP_results=$(createIP)
    { # 'try' block
    createInstance_results=$(createInstance)
    } || { # 'catch' block
    pushover "GCP Failure - $USERNAME" "$createIP_results $createInstance_results"
    exit 126
    }

    if [[ $OS == "ws"* ]]; then
      sleep 300
      echo $(setPassword)
    fi
    echo "$createIP_results"
    echo "$createInstance_results"
    pushover "GCP Success - $USERNAME created $INSTANCE_NAME" "$createInstance_results"
    e_success "The Devops team has been notified of this instance creation."

  else
    e_error "EXIT"
  fi
}

function gauth() {
  e_header "gcloud Auth"
  local service_account="$1"
  if [[ -f "/talend/$service_account" ]]; then
    gcloud auth activate-service-account --key-file="/talend/$service_account"
  else
    e_error "No Service Account /talend/<service_account>.json found"
    e_note "Run the container with -v c:/Users/<username>/<folder>:/talend."
    e_note "For a valid JSON Service Account contact support.devops@talend.com."
    exit 126
  fi

}


# Get username from system
function getUsername() {
  local sys_user
  local username

  sys_user=$(whoami)
  sys_user="${sys_user//[-._]/}"

if [[ $sys_user == 'root' ]]; then
  e_header "Case Info"
  echo -n "Talend username: "
  read username

  while ! [[ $username =~ ^[a-z]+$ ]]; do
    e_error "Invalid username"
    e_note "Only lowercase and no special characters."
    echo -n "Talend Username: "
    read username
  done
  USERNAME="$username"

else
  e_header "Case Info"
  echo -n "Talend username [$sys_user]: "
  read username

  if [[ $username == '' ]]; then # If username is empty
    USERNAME="$sys_user"
  else
    while ! [[ $username =~ ^[a-z]+$ ]]; do
      e_error "Invalid username"
      e_note "Only lowercase and no special characters."
      echo -n "Talend Username [$sys_user]: "
      read username
    done
    USERNAME="$username"
  fi
fi
}

function readSalesforceCase() {
  local salesforce_case

  echo -n "Salesforce Case Number: "
  read salesforce_case

  while ! [[ $salesforce_case =~ ^0[0-9]{7}$ ]] &&
        ! [[ $salesforce_case == 'support' ]] &&
        ! [[ $salesforce_case == 'rnd' ]] &&
        ! [[ $salesforce_case == 'devops' ]] &&
        ! [[ $salesforce_case == 'sales' ]] &&
        ! [[ $salesforce_case == 'escalations' ]]; do
    e_error "Invalid case number!"
    e_note "Salesforce case [00xxxxxx] or department [support] [rnd] [escalations] [devops] [sales]."
    echo -n "Salesforce case number: "
    read salesforce_case
  done
  SALESFORCE_CASE=$salesforce_case
}


function readTimeZone() {
  local time_selection
  e_header "User Time Zone"
  echo "Australia: [1] aedt, [2] aest"
  echo "Japan:     [3] jst"
  echo "China:     [4] cst"
  echo "Singapore: [5] sgt"
  echo "India:     [6] ist"
  echo "Europe:    [7] cet,  [8] cest, [9] gmt, [10] bsm"
  echo "America:   [11] est, [12] edt, [13] ct, [14] cdt, [15] pst, [16] pdt"
  echo -n "Select your time zone: "
  read time_selection

  while ! [[ $time_selection =~ ^([1-9]|1[0-6])$ ]]; do
    e_error "Invalid selection!"
    echo -n "Select a valid time zone [1 - 16]: "
    read time_selection
  done

  case "$time_selection" in
    '1') # "Australia: [1] aedt
      TIME_ZONE='aedt'
      ;;
    '2') # Australia [2] aest
      TIME_ZONE='aest'
      ;;
    '3') # Tokyo: [3] jst
      TIME_ZONE='jst'
      ;;
    '4') # Taiwan: [4] cst
      TIME_ZONE='cst'
      ;;
    '5') # Singapore: [5] sgt
      TIME_ZONE='sgt'
      ;;
    '6') # Mumbai: [6] ist
      TIME_ZONE='ist'
      ;;
    '7') # Europe: [7] cet
      TIME_ZONE='cst'
      ;;
    '8') # Europe: [8] cest
      TIME_ZONE='cest'
      ;;
    '9') # Europe: [9] gmt
      TIME_ZONE='gmt'
      ;;
    '10') # Europe: [10] bsm
      TIME_ZONE='bsm'
      ;;
    '11') # America: [11] est
      TIME_ZONE='est'
      ;;
    '12') # America: [12] edt
      TIME_ZONE='edt'
      ;;
    '13') # America: [13] ct
      TIME_ZONE='ct'
      ;;
    '14') # America: [14] cdt
      TIME_ZONE='cdt'
      ;;
    '15') # America: [15] pst,
      TIME_ZONE='pst'
      ;;
    '16') # America: [16] pdt
      TIME_ZONE='pdt'
      ;;
  esac
}

function readRegion() {
  local city_selection
  e_header "User City"
  e_subtitle "APAC"
  echo "[1] Bangalore,   [2] Beijing,   [3] Singapore, [4] Sydney,  [5] Tokyo"
  e_subtitle "EMEA"
  echo "[6] Alpnach,     [7] Barcelona, [8] Bonn,      [9] Feldkirchen"
  echo "[10] Maidenhead, [11] Milano,   [12] Nantes,   [13] Nürnberg,"
  echo "[14] Suresnes"
  e_subtitle "America"
  echo "[15] Atlanta,    [16] Boston,   [17] Montreal, [18] Irvine, [19] RWC"
  echo -n "Select your city: "
  read city_selection

  while ! [[ $city_selection =~ ^([1-9]|1[0-9])$ ]]; do
    e_error "Invalid city!"
    echo -n "Make a valid selection [1 - 19]: "
    read city_selection
  done

  case "$city_selection" in
    '1') # Bangalore
      REGION='asia-south1'
      ZONE='asia-south1-c'
      PROJECT='css-apac'
    ;;
    '2') # Beijing
      REGION='asia-east1'
      ZONE='asia-east1-a'
      PROJECT='css-apac'
    ;;
    '3') # Singapore
      REGION='asia-southeast1'
      ZONE='asia-southeast1-a'
      PROJECT='css-apac'
    ;;
    '4') # Sydney
      REGION='australia-southeast1'
      ZONE='australia-southeast1-a'
      PROJECT='css-apac'
    ;;
    '5') # Tokyo
      REGION='asia-northeast1'
      ZONE='australia-southeast1-a'
      PROJECT='css-apac'
    ;;
    '6'|'7'|'12'|'14') # Alpnach, Barcelona, Nantes, Suresnes
      REGION='europe-west1'
      ZONE='europe-west1-b'
      PROJECT='css-emea'
    ;;
    '8'|'9'|'11'|'13') # Bonn, Feldkirchen, Milano, Nürnberg
      REGION='europe-west3'
      ZONE='europe-west3-a'
      PROJECT='css-emea'
    ;;
    '10') # Maidenhead
      REGION='europe-west2'
      ZONE='europe-west2-c'
      PROJECT='css-emea'
    ;;
    '15') # Atlanta
      REGION='us-east1'
      ZONE='us-east1-b'
      PROJECT='css-us'
    ;;
    '16') # Boston
      REGION='us-east4'
      ZONE='us-east4-c'
      PROJECT='css-us'
    ;;
    '17') # Montreal
      REGION='northamerica-northeast1'
      ZONE='northamerica-northeast1-a'
      PROJECT='css-us'
    ;;
    '18'|'19') # Irvine, RWC
      REGION='us-west1'
      ZONE='us-west1-b'
      PROJECT='css-us'
    ;;
  esac
}

# Read image selection input from user
function readImage() {
  local image_selection
  e_header "GCP Image"
  echo "[1] Centos 7 (GUI), jdk8u151, Datafabric 601 to 701"
  echo "[2] Windows Server 2012 (GUI), jdk8u162, Datafabric 601 to 701"
  echo "[3] Ubuntu 14 (GUI), jdk8u171, Big Data and Studio 631"
  echo -n "Select an image: "
  read image_selection

  while ! [[ $image_selection =~ ^[1-3]$ ]]; do
    e_error "Invalid Image!"
    echo -n "Make a valid selection [1 - 3]: "
    read image_selection
  done

  case "$image_selection" in
    1)
      IMAGE_NAME='image-devops-centos7-jdk8u151-datafabric601-701'
      OS='centos7'
      readSoftware 'jdk8u151-datafabric601-701'
      IMAGE_PROJECT='css-us'
      MACHINE_TYPE='n1-standard-2'
      TAGS="$VNC,$HTTP,$HTTPS"
      BOOT_DISK_SIZE='120'
    ;;
    2)
      IMAGE_NAME='image-ws2012-datafabric601-to-701'
      OS='ws2012'
      readSoftware 'jdk8u162-datafabric601-701'
      IMAGE_PROJECT='css-us'
      MACHINE_TYPE='n1-standard-2'
      TAGS="$HTTP,$HTTPS"
      BOOT_DISK_SIZE='120'
    ;;
    3)
      OS='ubuntu14'
      IMAGE_NAME='image-ubuntu14-bigdata631-sandbox'
      readSoftware 'bigdata-studio631'
      IMAGE_PROJECT='css-us'
      MACHINE_TYPE='n1-standard-4'
      TAGS="$VNC,$HTTP,$HTTPS"
      BOOT_DISK_SIZE='100'
    ;;
  esac
}

# Read sofware name input from user
function readSoftware() {
  local software
  preset=$1
  e_header "Instance Software"
  echo -n "Software name for your instance [$preset]: "
  read software

  if [[ $software == '' ]]; then # If software is empty
    software="$preset"
  else
    while ! [[ "$software" =~ ^[a-z0-9\-]+$ ]]; do
      e_error "Invalid characters!"
      e_note "Only lowercase, numbers, and dashes accepted."
      e_note "Hit enter to use preset name [$preset]"
      echo -n "Software name for your instance: "
      read software
    done
  fi
  SOFTWARE="$software"
}


# Create IP
function createIP() {
  gcloud compute \
  addresses create "$IP_NAME" \
  --project "$PROJECT" \
  --region "$REGION"
}

# Create Instance
function createInstance() {
  gcloud beta compute \
  instances create "$INSTANCE_NAME" \
  --project "$PROJECT" \
  --zone "$ZONE" \
  --address="$IP_NAME" \
  --machine-type="$MACHINE_TYPE" \
  --subnet "default" \
  --maintenance-policy "MIGRATE" \
  --tags "$TAGS" \
  --labels "$LABELS" \
  --image "$IMAGE_NAME" \
  --image-project "$IMAGE_PROJECT" \
  --boot-disk-size "$BOOT_DISK_SIZE" \
  --boot-disk-type "pd-standard" \
  --boot-disk-device-name "$INSTANCE_NAME" \
  --min-cpu-platform "Automatic"
}

# Set Password
function setPassword() {
  echo Y | gcloud beta compute \
  reset-windows-password "$INSTANCE_NAME" \
  --project "$PROJECT" \
  --zone "$ZONE" \
  --user "talend"
}

function printValues() {
  e_header "Instance Details"
  echo "Instance: $INSTANCE_NAME"
  echo "Project: $PROJECT"
  echo "Zone: $ZONE"
  echo "Machine Type: $MACHINE_TYPE"
  echo "Tags: $TAGS"
  echo "Labels: $LABELS"
  echo "Image: $IMAGE_NAME"
  echo "Image Project: $IMAGE_PROJECT"
  echo "Disk Size: $BOOT_DISK_SIZE"
  echo "ssh username: talend"

  if ! [[ $OS == "ws" ]]; then
    echo "ssh keys: $PROJECT.ppk"
  else
    echo "Windows Password: <will be generated and printed below>"
  fi
}

main "$@"
